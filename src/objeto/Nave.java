/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objeto;

import game.Game;
import java.util.ArrayList;
import jplay.Sprite;

/**
 *
 * @author lucio
 */
public class Nave  extends Sprite {
    private int velocidade = 400;
    private double angulo = 0;
    private double time = 0;
    public ArrayList<Tiro> tiros =  new ArrayList(); 
    public Nave(String fileName, int width_windown, int height_windown) {
        super(fileName,36);
        this.x = width_windown/2 - this.width/2;
        this.y = height_windown/2 - this.height/2;   
    }
     public void move(){
        this.y -= velocidade / Game.get_delta_time() * Math.cos(Math.toRadians(angulo));
        this.x += velocidade / Game.get_delta_time() * Math.sin(Math.toRadians(angulo));
    }
     public void vira_esq(){
        time += 1/Game.get_delta_time()*10;
        if (time > 0.3){       
            this.angulo -= 10;
            if ((this.getCurrFrame())==35) this.setCurrFrame(-1);
            this.setCurrFrame( this.getCurrFrame()+ 1);
            System.out.println(this.angulo);
            this.time = 0;
        }
     }
     public void novo_tiro(){
         
         tiros.add(new Tiro("imagens/tiro.png", this.x + this.width/2, this.y + this.height/2, this.angulo));
         
     }
     public void vira_dir(){
        time += 1/Game.get_delta_time()*10;
        if (time > 0.3){       
            this.angulo += 10;
            System.out.println(this.angulo);
            if ((this.getCurrFrame())==0) this.setCurrFrame(36);
            this.setCurrFrame( this.getCurrFrame()- 1);
            System.out.println(this.angulo);
            
//            System.out.println(Math.cos(this.angulo*(Math.PI/180)));
//            System.out.println(Math.cos(this.angulo*(Math.PI/180)));
            this.time = 0;
        }
     }
     public void move_tiro(){
         int n = this.tiros.size();
         
         for (int i = 0; i<n ; i++){
             if (this.tiros.get(i)==null) continue;
             this.tiros.get(i).move();
             this.tiros.get(i).draw();
             if (this.tiros.get(i).y < 0){
                this.tiros.set(i, null);
             }
             
         }
     }
     
     
    
     }

    

