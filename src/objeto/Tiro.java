/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objeto;

import game.Game;
import jplay.Sprite;

/**
 *
 * @author lucio
 */
public class Tiro extends Sprite {
    private int velocidade = 600;
    private double angulo;
    public Tiro(String fileName,double x ,double y,double angulo) {
        super(fileName);
        this.x = x;
        this.y = y;
        this.angulo = angulo;
    }

    
    public void move(){
        this.y -= this.velocidade / Game.get_delta_time() * Math.cos(Math.toRadians(this.angulo));
        this.x += this.velocidade / Game.get_delta_time() * Math.sin(Math.toRadians(this.angulo));
    }
    
    
    
    
}
