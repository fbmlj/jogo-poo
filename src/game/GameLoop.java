/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import jplay.Keyboard;
import objeto.Nave;

/**
 *
 * @author lucio
 */
public class GameLoop {
    Nave nave;
    Keyboard teclado = Game.w.getKeyboard();
    
    
    public GameLoop(){
        nave = new Nave("imagens/nave_animacao.png",Game.width,Game.height);
        
        
    }
    
    public void game(){
        
        if (teclado.keyDown(Keyboard.UP_KEY))
        this.nave.move();
        if (teclado.keyDown(Keyboard.LEFT_KEY)){
            this.nave.vira_esq();
        }
        if (teclado.keyDown(Keyboard.RIGHT_KEY)){
            this.nave.vira_dir();
        }
       if (teclado.keyDown(Keyboard.SPACE_KEY))
           this.nave.novo_tiro();
        this.nave.move_tiro();
        this.nave.draw();
        
    }
    
   
    
}
