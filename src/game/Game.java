/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game;
import java.awt.Color;
import jplay.Window;
import jplay.Keyboard;
import jplay.GameImage;


/**
 *
 * @author lucio
 */
public class Game {
    public static int width = 1000;
    public static int height = 900;
    static public Window w = new Window(width, height);
public static void main(String[] args)  
    {   
        GameLoop game =  new GameLoop();
        while(true)  
        {  
            
            w.clear(Color.black);
            game.game();
  
            //This command must be run at last.  
            w.update();  
        }
    }
    public static double get_delta_time(){
        return w.deltaTime();
    }

    
}
